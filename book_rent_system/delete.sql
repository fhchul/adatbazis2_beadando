
drop SEQUENCE book_id_seq;
drop SEQUENCE category_id_seq;
drop SEQUENCE user_id_seq;
drop SEQUENCE location_id_seq;
drop SEQUENCE rent_id_seq;

drop table book_categories_h;
drop table book_locations_h;
drop table users_h;
drop table rentable_books_h;
drop table rental_details_h;

drop table book_categories;
drop table book_locations;
drop table users;
drop table rentable_books;
drop table rental_details;

drop view vw_book_details;
drop view vw_book_location_details;
drop view vw_rental_informations;
