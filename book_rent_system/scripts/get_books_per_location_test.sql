declare
    bpl_array ty_bpl_array;
begin
    bpl_array := get_data_pkg.get_books_per_location(p_location_id => 10100);
    for i in 1 .. bpl_array.count
        loop
        dbms_output.put_line('Book ID: ' || bpl_array(i).p_book_id || ', ' ||
                             'Book: ' || bpl_array(i).p_author || ' ' ||
                             bpl_array(i).p_book_title);
        end loop;
    if bpl_array.count <= 0 then dbms_output.put_line('Nincs ilyen konyv az adott helyen vagy a hely nem letezik!');
    end if;
end;
