begin
    add_book_category('Egyeni', 30);
    add_book_location('Pecs', 'Szigeti ut', 6, 7624);
    add_user('Zsombor', 'Havas', 'ZsomborHavas@gmaileeey.com', '06-99-333-0202', '2000.09.22');
    add_rentable_book(20, 'Magam pszichologiaja', 'Havas Zsombor', 'Tarsadalomtudomany', 'Hungary', 501, 5, 2021, 10200);
    add_rental_detail(8, 113, '2022.12.15', '2022.12.30');
end;
