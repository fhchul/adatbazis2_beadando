declare
    book_array ty_book_array;
begin
    book_array := get_data_pkg.get_books;
    for i in 1 .. book_array.count
        loop
        dbms_output.put_line(book_array(i).p_author || ' ' || 
                             book_array(i).p_book_title || ', ' ||
                             'Topic: ' || book_array(i).p_topic || ', ' ||
                             'Country: ' || book_array(i).p_country || ', ' ||
                             'Page: ' || book_array(i).p_page || ', ' ||
                             'Points: ' || book_array(i).p_points || ', ' ||
                             'Year: ' || book_array(i).p_year);
        end loop;
end;
