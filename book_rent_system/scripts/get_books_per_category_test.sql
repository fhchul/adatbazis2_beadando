declare
    bpc_array ty_bpc_array;
begin
    bpc_array := get_data_pkg.get_books_per_category(p_category_id => 20);
    for i in 1 .. bpc_array.count
        loop
        dbms_output.put_line('Category: ' || bpc_array(i).p_category_name || ', ' || 
                             'Book ID: ' || bpc_array(i).p_book_id || ', ' ||
                             'Book: ' || bpc_array(i).p_author || ' ' ||
                             bpc_array(i).p_book_title);
        end loop;
    if bpc_array.count <= 0 then dbms_output.put_line('Nincs ilyen konyv a kategoriaban vagy nem is letezik ilyen kategoria!');
    end if;
end;
