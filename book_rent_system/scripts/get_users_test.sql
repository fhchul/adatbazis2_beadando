declare
    user_array ty_user_array;
begin
    user_array := get_data_pkg.get_users;
    for i in 1 .. user_array.count
        loop
        dbms_output.put_line(user_array(i).p_first_name || ' ' ||
                             user_array(i).p_last_name || ', ' ||
                             'E-mail: ' || user_array(i).p_email || ', ' ||
                             'Telefon: ' || user_array(i).p_telefon || ', ' ||
                             'Birth date: ' || user_array(i).p_birth_date);
        end loop;
end;
