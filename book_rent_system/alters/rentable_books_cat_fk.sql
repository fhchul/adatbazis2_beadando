ALTER TABLE rentable_books
ADD CONSTRAINT rentable_books_cat_fk FOREIGN KEY (category_id) REFERENCES book_categories(category_id);
