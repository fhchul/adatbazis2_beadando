ALTER TABLE rentable_books
ADD CONSTRAINT rentable_books_loc_fk FOREIGN KEY (location_id) REFERENCES book_locations(location_id);
