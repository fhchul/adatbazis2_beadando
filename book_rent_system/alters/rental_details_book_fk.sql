ALTER TABLE rental_details
ADD CONSTRAINT rental_details_book_fk FOREIGN KEY (book_id) REFERENCES rentable_books(book_id);
