CREATE OR REPLACE PROCEDURE add_book_category(p_category_name VARCHAR2
                                             ,p_cost_per_day  NUMBER) IS
BEGIN
  INSERT INTO book_categories
    (category_name
    ,cost_per_day)
  VALUES
    (p_category_name
    ,p_cost_per_day);
END;
