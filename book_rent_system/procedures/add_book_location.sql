CREATE OR REPLACE PROCEDURE add_book_location(p_city            VARCHAR2
                                             ,p_street_name     VARCHAR2
                                             ,p_building_number NUMBER
                                             ,p_zip_code        NUMBER) IS
BEGIN
  INSERT INTO book_locations
    (city
    ,street_name
    ,building_number
    ,zip_code)
  VALUES
    (p_city
    ,p_street_name
    ,p_building_number
    ,p_zip_code);
END;
