CREATE OR REPLACE PROCEDURE add_user(p_first_name VARCHAR2
                                    ,p_last_name  VARCHAR2
                                    ,p_email      VARCHAR2
                                    ,p_telefon    VARCHAR2
                                    ,p_birth_date VARCHAR2) IS
BEGIN
  INSERT INTO users
    (first_name
    ,last_name
    ,email
    ,telefon
    ,birth_date)
  VALUES
    (p_first_name
    ,p_last_name
    ,p_email
    ,p_telefon
    ,to_date(p_birth_date, 'YYYY-MM-DD'));
END;
