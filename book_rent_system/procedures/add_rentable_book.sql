CREATE OR REPLACE PROCEDURE add_rentable_book(p_category_id NUMBER
                                             ,p_book_title  VARCHAR2
                                             ,p_author      VARCHAR2
                                             ,p_topic       VARCHAR2
                                             ,p_country     VARCHAR2
                                             ,p_page        NUMBER
                                             ,p_points      NUMBER
                                             ,p_year        NUMBER
                                             ,p_location_id NUMBER) IS
  p_category_not_found NUMBER;
  p_location_not_found NUMBER;
  category_not_found EXCEPTION;
  location_not_found EXCEPTION;
  PRAGMA EXCEPTION_INIT(category_not_found, -20001);
  PRAGMA EXCEPTION_INIT(location_not_found, -20002);

BEGIN
  SELECT COUNT(*)
    INTO p_category_not_found
    FROM book_categories
   WHERE category_id = p_category_id;

  SELECT COUNT(*)
    INTO p_location_not_found
    FROM book_locations
   WHERE location_id = p_location_id;

  IF p_category_not_found < 1
  THEN
    raise_application_error(-20001, 'Nincs ilyen kategoria.');
  
  ELSIF p_location_not_found < 1
  THEN
    raise_application_error(-20002, 'Nincs ilyen hely.');
  
  ELSE
  
    INSERT INTO rentable_books
      (category_id
      ,book_title
      ,author
      ,topic
      ,country
      ,page
      ,points
      ,year
      ,location_id)
    VALUES
      (p_category_id
      ,p_book_title
      ,p_author
      ,p_topic
      ,p_country
      ,p_page
      ,p_points
      ,p_year
      ,p_location_id);
  
  END IF;
END;
