CREATE OR REPLACE PROCEDURE add_rental_detail(p_user_id    NUMBER
                                             ,p_book_id    VARCHAR2
                                             ,p_start_date VARCHAR2
                                             ,p_end_date   VARCHAR2) IS
  p_user_not_found NUMBER;
  p_book_not_found NUMBER;
  user_not_found EXCEPTION;
  book_not_found EXCEPTION;
  PRAGMA EXCEPTION_INIT(user_not_found, -20003);
  PRAGMA EXCEPTION_INIT(book_not_found, -20004);

BEGIN
  SELECT COUNT(*)
    INTO p_book_not_found
    FROM rentable_books
   WHERE book_id = p_book_id;

  SELECT COUNT(*)
    INTO p_user_not_found
    FROM users
   WHERE user_id = p_user_id;

  IF p_book_not_found < 1
  THEN
    raise_application_error(-20004, 'A konyv nem talalhato.');
  
  ELSIF p_user_not_found < 1
  THEN
    raise_application_error(-20003, 'A felhasznalo nem talalhato.');
  
  ELSE
  
    INSERT INTO rental_details
      (user_id
      ,book_id
      ,start_date
      ,end_date)
    VALUES
      (p_user_id
      ,p_book_id
      ,to_date(p_start_date, 'YYYY-MM-DD')
      ,to_date(p_end_date, 'YYYY-MM-DD'));
  
  END IF;
END;
