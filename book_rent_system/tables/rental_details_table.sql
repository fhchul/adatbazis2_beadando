create table rental_details (
    rent_id number not null,
    user_id number not null,
    book_id number not null,
    start_date date not null,
    end_date date not null,
    last_modified date default sysdate not null,
    created_date date default sysdate not null,
    mod_user varchar2(300),
    dml_flag varchar(1),
    version number
);
