create table users (
    user_id number not null,
    first_name varchar2(50) not null,
    last_name varchar2(50) not null,
    email varchar2(80) not null,
    telefon varchar2 (20) not null,
    birth_date date not null,
    last_modified date default sysdate not null,
    created_date date default sysdate not null,
    mod_user varchar2(300),
    dml_flag varchar(1),
    version number
);
