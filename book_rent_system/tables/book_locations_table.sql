create table book_locations (
    location_id number not null,
    city varchar2(50) not null,
    street_name varchar2(100) not null,
    building_number number not null,
    zip_code number not null,
    last_modified date default sysdate not null,
    created_date date default sysdate not null,
    mod_user varchar2(300),
    dml_flag varchar(1),
    version number
);
