create table rental_details_h (
    rent_id number,
    user_id number,
    book_id number,
    start_date date,
    end_date date,
    last_modified date,
    created_date date,
    mod_user varchar2(300),
    dml_flag varchar(1),
    version number
);
