create table rentable_books_h (
    book_id number,
    category_id number,
    book_title varchar2(50),
    author varchar2(50),
    topic varchar2(50),
    country varchar2(50),
    page number,
    points number,
    year number,
    location_id number,
    last_modified date,
    created_date date,
    mod_user varchar2(300),
    dml_flag varchar(1),
    version number
);
