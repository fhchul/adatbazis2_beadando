create table rentable_books (
    book_id number not null,
    category_id number not null,
    book_title varchar2(50) not null,
    author varchar2(50) not null,
    topic varchar2(50) not null,
    country varchar2(50) not null,
    page number not null,
    points number not null,
    year number not null,
    location_id number not null,
    last_modified date default sysdate not null,
    created_date date default sysdate not null,
    mod_user varchar2(300),
    dml_flag varchar(1),
    version number
);
