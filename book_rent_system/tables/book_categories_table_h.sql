create table book_categories_h (
    category_id number,
    category_name varchar2(50),
    cost_per_day number,
    last_modified date,
    created_date date,
    mod_user varchar2(300),
    dml_flag varchar(1),
    version number
);
