CREATE TRIGGER location_id_seq_trigger
  BEFORE INSERT ON book_locations
  FOR EACH ROW
BEGIN
  IF (:new.location_id IS NULL)
  THEN
    :new.location_id := location_id_seq.nextval;
  END IF;
END;
