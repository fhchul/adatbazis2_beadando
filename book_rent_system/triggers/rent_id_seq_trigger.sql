CREATE TRIGGER rent_id_seq_trigger
  BEFORE INSERT ON rental_details
  FOR EACH ROW
BEGIN
  IF (:new.rent_id IS NULL)
  THEN
    :new.rent_id := rent_id_seq.nextval;
  END IF;
END;
