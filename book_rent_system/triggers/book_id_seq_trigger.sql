CREATE TRIGGER book_id_seq_trigger
  BEFORE INSERT ON rentable_books
  FOR EACH ROW
BEGIN
  IF (:new.book_id IS NULL)
  THEN
    :new.book_id := book_id_seq.nextval;
  END IF;
END;
