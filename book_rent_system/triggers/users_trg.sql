CREATE OR REPLACE TRIGGER users_trg
  BEFORE INSERT OR UPDATE ON users
  FOR EACH ROW
BEGIN
  IF (inserting)
  THEN
    IF (:new.user_id IS NULL)
    THEN
      :new.user_id := user_id_seq.nextval;
    END IF;
    :new.created_date  := SYSDATE;
    :new.last_modified := SYSDATE;
    :new.dml_flag      := 'I';
    :new.version       := 1;
    :new.mod_user      := sys_context(namespace => 'USERENV',
                                      attribute => 'OS_USER');
  ELSE
    :new.last_modified := SYSDATE;
    :new.dml_flag      := 'U';
    :new.mod_user      := sys_context(namespace => 'USERENV',
                                      attribute => 'OS_USER');
    :new.version       := :old.version + 1;
  END IF;
END;
/

create or replace trigger users_h_trg
after delete or update or insert on users
for each row
begin
    if (deleting) then
       insert into users_h(user_id,
                              first_name,
                              last_name,
                              email,
                              telefon,
                              birth_date,
                              last_modified,
                              created_date,
                              mod_user,
                              dml_flag,
                              version)
                              values(
                             :old.user_id, 
                             :old.first_name, 
                             :old.last_name, 
                             :old.email, 
                             :old.telefon, 
                             :old.birth_date,
                             sysdate, 
                             :old.created_date,
                             sys_context('USERENV','OS_USER'),
                             'D', 
                             :old.version + 1);
    else
       insert into users_h(user_id,
                              first_name,
                              last_name,
                              email,
                              telefon,
                              birth_date,
                              last_modified,
                              created_date,
                              mod_user,
                              dml_flag,
                              version)
                              values(
                              :new.user_id,
                              :new.first_name,
                              :new.last_name,
                              :new.email,
                              :new.telefon,
                              :new.birth_date,
                              :new.last_modified,
                              :new.created_date,
                              sys_context('USERENV','OS_USER'),
                              :new.dml_flag,
                              :new.version
                              );
      end if;
end;
