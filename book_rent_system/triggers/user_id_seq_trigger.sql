CREATE TRIGGER user_id_seq_trigger
  BEFORE INSERT ON users
  FOR EACH ROW
BEGIN
  IF (:new.user_id IS NULL)
  THEN
    :new.user_id := user_id_seq.nextval;
  END IF;
END;
