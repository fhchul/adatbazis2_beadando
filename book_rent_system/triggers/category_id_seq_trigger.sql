CREATE TRIGGER category_id_seq_trigger
  BEFORE INSERT ON book_categories
  FOR EACH ROW
BEGIN
  IF (:new.category_id IS NULL)
  THEN
    :new.category_id := category_id_seq.nextval;
  END IF;
END;
