CREATE OR REPLACE TRIGGER rentable_books_trg
  BEFORE INSERT OR UPDATE ON rentable_books
  FOR EACH ROW
BEGIN
  IF (inserting)
  THEN
    IF (:new.book_id IS NULL)
    THEN
      :new.book_id := book_id_seq.nextval;
    END IF;
    :new.created_date  := SYSDATE;
    :new.last_modified := SYSDATE;
    :new.dml_flag      := 'I';
    :new.version       := 1;
    :new.mod_user      := sys_context(namespace => 'USERENV',
                                      attribute => 'OS_USER');
  ELSE
    :new.last_modified := SYSDATE;
    :new.dml_flag      := 'U';
    :new.mod_user      := sys_context(namespace => 'USERENV',
                                      attribute => 'OS_USER');
    :new.version       := :old.version + 1;
  END IF;
END;
/

create or replace trigger rentable_books_h_trg
after delete or update or insert on rentable_books
for each row
begin
    if (deleting) then
       insert into rentable_books_h(book_id,
                              category_id,
                              book_title,
                              author,
                              topic,
                              country,
                              page,
                              points,
                              year,
                              location_id,
                              last_modified,
                              created_date,
                              mod_user,
                              dml_flag,
                              version)
                              values(
                             :old.book_id, 
                             :old.category_id, 
                             :old.book_title, 
                             :old.author, 
                             :old.topic, 
                             :old.country,
                             :old.page,
                             :old.points,
                             :old.year,
                             :old.location_id,
                             sysdate, 
                             :old.created_date,
                             sys_context('USERENV','OS_USER'),
                             'D', 
                             :old.version + 1);
    else
       insert into rentable_books_h(book_id,
                              category_id,
                              book_title,
                              author,
                              topic,
                              country,
                              page,
                              points,
                              year,
                              location_id,
                              last_modified,
                              created_date,
                              mod_user,
                              dml_flag,
                              version)
                              values(
                              :new.book_id,
                              :new.category_id,
                              :new.book_title,
                              :new.author,
                              :new.topic,
                              :new.country,
                              :new.page,
                              :new.points,
                              :new.year,
                              :new.location_id,
                              :new.last_modified,
                              :new.created_date,
                              sys_context('USERENV','OS_USER'),
                              :new.dml_flag,
                              :new.version
                              );
      end if;
end;
