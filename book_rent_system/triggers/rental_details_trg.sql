CREATE OR REPLACE TRIGGER rental_details_trg
  BEFORE INSERT OR UPDATE ON rental_details
  FOR EACH ROW
BEGIN
  IF (inserting)
  THEN
    IF (:new.rent_id IS NULL)
    THEN
      :new.rent_id := rent_id_seq.nextval;
    END IF;
    :new.created_date  := SYSDATE;
    :new.last_modified := SYSDATE;
    :new.dml_flag      := 'I';
    :new.version       := 1;
    :new.mod_user      := sys_context(namespace => 'USERENV',
                                      attribute => 'OS_USER');
  ELSE
    :new.last_modified := SYSDATE;
    :new.dml_flag      := 'U';
    :new.mod_user      := sys_context(namespace => 'USERENV',
                                      attribute => 'OS_USER');
    :new.version       := :old.version + 1;
  END IF;
END;
/

create or replace trigger rental_details_h_trg
after delete or update or insert on rental_details
for each row
begin
    if (deleting) then
       insert into rental_details_h(rent_id,
                              user_id,
                              book_id,
                              start_date,
                              end_date,
                              last_modified,
                              created_date,
                              mod_user,
                              dml_flag,
                              version)
                              values(
                             :old.rent_id, 
                             :old.user_id, 
                             :old.book_id, 
                             :old.start_date, 
                             :old.end_date, 
                             sysdate, 
                             :old.created_date,
                             sys_context('USERENV','OS_USER'),
                             'D', 
                             :old.version + 1);
    else
       insert into rental_details_h(rent_id,
                              user_id,
                              book_id,
                              start_date,
                              end_date,
                              last_modified,
                              created_date,
                              mod_user,
                              dml_flag,
                              version)
                              values(
                              :new.rent_id,
                              :new.user_id,
                              :new.book_id,
                              :new.start_date,
                              :new.end_date,
                              :new.last_modified,
                              :new.created_date,
                              sys_context('USERENV','OS_USER'),
                              :new.dml_flag,
                              :new.version
                              );
      end if;
end;
