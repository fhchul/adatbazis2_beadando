CREATE OR REPLACE TRIGGER book_locations_trg
  BEFORE INSERT OR UPDATE ON book_locations
  FOR EACH ROW
BEGIN
  IF (inserting)
  THEN
    IF (:new.location_id IS NULL)
    THEN
      :new.location_id := location_id_seq.nextval;
    END IF;
    :new.created_date  := SYSDATE;
    :new.last_modified := SYSDATE;
    :new.dml_flag      := 'I';
    :new.version       := 1;
    :new.mod_user      := sys_context(namespace => 'USERENV',
                                      attribute => 'OS_USER');
  ELSE
    :new.last_modified := SYSDATE;
    :new.dml_flag      := 'U';
    :new.mod_user      := sys_context(namespace => 'USERENV',
                                      attribute => 'OS_USER');
    :new.version       := :old.version + 1;
  END IF;
END;
/

create or replace trigger book_locations_h_trg
after delete or update or insert on book_locations
for each row
begin
    if (deleting) then
       insert into book_locations_h(location_id,
                              city,
                              street_name,
                              building_number,
                              zip_code,
                              last_modified,
                              created_date,
                              mod_user,
                              dml_flag,
                              version)
                              values(
                             :old.location_id, 
                             :old.city, 
                             :old.street_name, 
                             :old.building_number, 
                             :old.zip_code,
                             sysdate, 
                             :old.created_date,
                             sys_context('USERENV','OS_USER'),
                             'D', 
                             :old.version + 1);
    else
       insert into book_locations_h(location_id,
                              city,
                              street_name,
                              building_number,
                              zip_code,
                              last_modified,
                              created_date,
                              mod_user,
                              dml_flag,
                              version)
                              values(
                              :new.location_id,
                              :new.city,
                              :new.street_name,
                              :new.building_number,
                              :new.zip_code,
                              :new.last_modified,
                              :new.created_date,
                              sys_context('USERENV','OS_USER'),
                              :new.dml_flag,
                              :new.version
                              );
      end if;
end;
