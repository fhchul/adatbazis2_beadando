INSERT INTO users(first_name, last_name, email, telefon, birth_date)
VALUES('Robert', 'Kiss', 'RobertKiss@gmailye.com', '06-99-123-4567', to_date('1988-06-21', 'yyyy-mm-dd'));

INSERT INTO users(first_name, last_name, email, telefon, birth_date)
VALUES('Tibor', 'Nagy', 'TiborNagy@gmailye.com', '06-99-989-9898', to_date('1976-10-09', 'yyyy-mm-dd'));

INSERT INTO users(first_name, last_name, email, telefon, birth_date)
VALUES('Anna', 'Voros', 'AnnaVoros@gmailye.com', '06-99-555-5555', to_date('2001-01-16', 'yyyy-mm-dd'));

INSERT INTO users(first_name, last_name, email, telefon, birth_date)
VALUES('Karoly', 'Piros', 'KarolyPiros@gmailye.com', '06-99-222-2222', to_date('1995-11-21', 'yyyy-mm-dd'));

INSERT INTO users(first_name, last_name, email, telefon, birth_date)
VALUES('Fruzsina', 'Bajnok', 'FruzsinaBajnok@gmailye.com', '06-99-666-7777', to_date('2000-05-09', 'yyyy-mm-dd'));

INSERT INTO users(first_name, last_name, email, telefon, birth_date)
VALUES('Tamas', 'Kemeny', 'TamasKemeny@gmailye.com', '06-99-000-0000', to_date('1989-04-23', 'yyyy-mm-dd'));

INSERT INTO users(first_name, last_name, email, telefon, birth_date)
VALUES('Julia', 'Stark', 'JuliaStark@gmailye.com', '06-99-111-1111', to_date('1998-08-13', 'yyyy-mm-dd'));
