INSERT INTO rentable_books(category_id, book_title, author, topic, country, page, points, year, location_id)
VALUES(10, 'Az Intezet', 'Stephen King', 'Sci-fi, fantasy, krimi', 'USA', 496, 4, 2020, 10100);

INSERT INTO rentable_books(category_id, book_title, author, topic, country, page, points, year, location_id)
VALUES(10, 'Ahol a folyami rakok enekelnek', 'Delia Owens', 'Felnottirodalom', 'USA', 464, 5, 2022, 10200);

INSERT INTO rentable_books(category_id, book_title, author, topic, country, page, points, year, location_id)
VALUES(20, 'Normalis vagy', 'Dr Mate Gabor', 'Tarsadalomtudomany', 'Hungary', 592, 5, 2022, 10300);

INSERT INTO rentable_books(category_id, book_title, author, topic, country, page, points, year, location_id)
VALUES(20, 'Budapest ostroma', 'Ungvary Krisztian', 'Tortenelem', 'Hungary', 364, 4, 2013, 10400);

INSERT INTO rentable_books(category_id, book_title, author, topic, country, page, points, year, location_id)
VALUES(30, 'A szerzoi jog kalozai', 'Bodo Balazs', 'Szamitastechnikai', 'Hungary', 329, 1, 2011, 10100);

INSERT INTO rentable_books(category_id, book_title, author, topic, country, page, points, year, location_id)
VALUES(30, 'Orvosi biotechnologia', 'Ongradi Jozsef', 'Orvosi', 'Hungary', 280, 5, 2022, 10200);

INSERT INTO rentable_books(category_id, book_title, author, topic, country, page, points, year, location_id)
VALUES(40, 'Kozgazdasagtan lelkes amatoroknek', 'Kertesz Balazs', 'Kozgazdasag', 'Hungary', 224, 5, 2022, 10100);

INSERT INTO rentable_books(category_id, book_title, author, topic, country, page, points, year, location_id)
VALUES(40, 'Az eladas pszichologiaja', 'Brian Tracy', 'Menedzserkonyv', 'Canada', 22500, 5, 2022, 10300);

INSERT INTO rentable_books(category_id, book_title, author, topic, country, page, points, year, location_id)
VALUES(50, 'Meinig Artur', 'Rozsnyai Jozsef', 'Epiteszet', 'Hungary', 256, 5, 2018, 10400);

INSERT INTO rentable_books(category_id, book_title, author, topic, country, page, points, year, location_id)
VALUES(50, 'Tanchaz 50', 'Javorszky Bela Szilard', 'Tanc', 'Hungary', 208, 5, 2022, 10400);

INSERT INTO rentable_books(category_id, book_title, author, topic, country, page, points, year, location_id)
VALUES(60, 'A magyar nyelv alaktana', 'M Korchmaros Valeria', 'Foiskola, egyetem', 'Hungary', 256, 5, 2021, 10500);

INSERT INTO rentable_books(category_id, book_title, author, topic, country, page, points, year, location_id)
VALUES(60, 'Biologia II', 'Fazekas Gyorgy, Szerenyi Gabor', 'Kozepiskola', 'Hungary', 592, 5, 2022, 10500);
