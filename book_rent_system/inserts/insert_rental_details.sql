INSERT INTO rental_details(user_id, book_id, start_date, end_date)
VALUES(2, 108, to_date('2022-12-02', 'yyyy-mm-dd'), to_date('2022-12-11', 'yyyy-mm-dd'));

INSERT INTO rental_details(user_id, book_id, start_date, end_date)
VALUES(4, 110, to_date('2022-12-10', 'yyyy-mm-dd'), to_date('2022-12-15', 'yyyy-mm-dd'));

INSERT INTO rental_details(user_id, book_id, start_date, end_date)
VALUES(2, 101, to_date('2022-12-04', 'yyyy-mm-dd'), to_date('2022-12-08', 'yyyy-mm-dd'));

INSERT INTO rental_details(user_id, book_id, start_date, end_date)
VALUES(6, 107, to_date('2022-12-21', 'yyyy-mm-dd'), to_date('2022-12-29', 'yyyy-mm-dd'));

INSERT INTO rental_details(user_id, book_id, start_date, end_date)
VALUES(7, 109, to_date('2022-12-16', 'yyyy-mm-dd'), to_date('2022-12-30', 'yyyy-mm-dd'));

INSERT INTO rental_details(user_id, book_id, start_date, end_date)
VALUES(1, 104, to_date('2022-12-23', 'yyyy-mm-dd'), to_date('2022-12-29', 'yyyy-mm-dd'));
