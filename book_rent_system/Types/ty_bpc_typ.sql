CREATE OR REPLACE TYPE ty_bpc AS OBJECT
(
  p_category_name VARCHAR2(50),
  p_book_id       NUMBER,
  p_author        VARCHAR2(50),
  p_book_title    VARCHAR2(50)
)
