CREATE OR REPLACE TYPE ty_book AS OBJECT
(
  p_author     VARCHAR2(50),
  p_book_title VARCHAR2(50),
  p_topic      VARCHAR2(50),
  p_country    VARCHAR2(50),
  p_page       NUMBER,
  p_points     NUMBER,
  p_year       NUMBER
)
