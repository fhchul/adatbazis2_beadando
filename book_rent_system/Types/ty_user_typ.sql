CREATE OR REPLACE TYPE ty_user AS OBJECT
(
  p_first_name VARCHAR2(50),
  p_last_name  VARCHAR2(50),
  p_email      VARCHAR2(80),
  p_telefon    VARCHAR2(20),
  p_birth_date DATE
)
