CREATE OR REPLACE PACKAGE get_data_pkg IS

  FUNCTION get_books RETURN ty_book_array;

  FUNCTION get_books_per_location(p_location_id IN NUMBER)
    RETURN ty_bpl_array;

  FUNCTION get_books_per_category(p_category_id IN NUMBER)
    RETURN ty_bpc_array;

  FUNCTION get_users RETURN ty_user_array;

END;
/
CREATE OR REPLACE PACKAGE BODY get_data_pkg IS

  function get_books_per_location(p_location_id number)
    return ty_bpl_array is
    bpl_array ty_bpl_array;
  begin
    bpl_array := ty_bpl_array();
    for i in (select rb.book_id,
                     rb.author,
                     rb.book_title
                from rentable_books rb join book_locations bl on (rb.location_id = bl.location_id)
               where rb.location_id = p_location_id) loop
      bpl_array.extend();
      bpl_array(bpl_array.count) := ty_bpl(p_book_id => i.book_id,
                                           p_author => i.author,
                                           p_book_title => i.book_title);
    end loop;
    return bpl_array;
  end;

  function get_books_per_category(p_category_id number)
    return ty_bpc_array is
    bpc_array ty_bpc_array;
  begin
    bpc_array := ty_bpc_array();
    for i in (select bc.category_name,
                     rb.book_id,
                     rb.author,
                     rb.book_title
                from rentable_books rb join book_categories bc on (rb.category_id = bc.category_id)
               where rb.category_id = p_category_id) loop
      bpc_array.extend();
      bpc_array(bpc_array.count) := ty_bpc(p_category_name => i.category_name,
                                           p_book_id => i.book_id,
                                           p_author => i.author,
                                           p_book_title => i.book_title);
    end loop;
    return bpc_array;
  end;

  function get_users return ty_user_array is
           user_array ty_user_array;
    begin
      user_array := ty_user_array();
      for i in (select first_name, last_name, email, telefon, birth_date
                from users)
        loop
          user_array.extend();
          user_array(user_array.count) := ty_user(
                                        p_first_name => i.first_name,
                                        p_last_name => i.last_name,
                                        p_email => i.email,
                                        p_telefon => i.telefon,
                                        p_birth_date => i.birth_date);
        end loop;
    return user_array;
    end;

  function get_books return ty_book_array is
           book_array ty_book_array;
    begin
      book_array := ty_book_array();
      for i in (select author, book_title, topic, country, page, points, year
                from rentable_books)
        loop
          book_array.extend();
          book_array(book_array.count) := ty_book(
                                        p_author => i.author,
                                        p_book_title => i.book_title,
                                        p_topic => i.topic,
                                        p_country => i.country,
                                        p_page => i.page,
                                        p_points => i.points,
                                        p_year => i.year);
        end loop;
    return book_array;
    end;
end;
