SET SERVEROUTPUT ON;


PROMPT Installing DB...

PROMPT installing SEQUENCES...
@./sequences/book_id_seq.sql
@./sequences/category_id_seq.sql
@./sequences/user_id_seq.sql
@./sequences/location_id_seq.sql
@./sequences/rent_id_seq.sql

PROMPT installing TABLES...
@./tables/book_categories_table.sql
@./tables/book_locations_table.sql
@./tables/users_table.sql
@./tables/rentable_books_table.sql
@./tables/rental_details_table.sql
@./tables/book_categories_table_h.sql
@./tables/book_locations_table_h.sql
@./tables/users_table_h.sql
@./tables/rentable_books_table_h.sql
@./tables/rental_details_table_h.sql

PROMPT installig TYPES...
@./Types/ty_book_typ.sql
@./Types/ty_user_typ.sql
@./Types/ty_bpc_typ.sql
@./Types/ty_bpl_typ.sql
@./Types/ty_book_array_typ.sql
@./Types/ty_user_array_typ.sql
@./Types/ty_bpc_array_typ.sql
@./Types/ty_bpl_array_typ.sql

PROMPT installing PACKAGES...
@./packages/get_data_pkg.sql

PROMPT installing PROCEDURES...
@./procedures/add_book_category.sql
@./procedures/add_book_location.sql
@./procedures/add_user.sql
@./procedures/add_rentable_book.sql
@./procedures/add_rental_detail.sql

PROMPT installing VIEWS...
@./views/vw_book_details.sql
@./views/vw_book_location_details.sql
@./views/vw_rental_informations.sql

PROMPT installing TRIGGERS...
@./triggers/book_id_seq_trigger.sql
@./triggers/category_id_seq_trigger.sql
@./triggers/location_id_seq_trigger.sql
@./triggers/rent_id_seq_trigger.sql
@./triggers/user_id_seq_trigger.sql
@./triggers/book_categories_trg.sql
@./triggers/book_locations_trg.sql
@./triggers/users_trg.sql
@./triggers/rental_details_trg.sql
@./triggers/rentable_books_trg.sql

PROMPT installing ALTERS...
@./alters/book_categories_pk.sql
@./alters/book_location_pk.sql
@./alters/users_pk.sql
@./alters/rentable_books_pk.sql
@./alters/rental_details_pk.sql
@./alters/rentable_books_cat_fk.sql
@./alters/rentable_books_loc_fk.sql
@./alters/rental_details_book_fk.sql
@./alters/rental_details_user_fk.sql

COMMIT;

BEGIN
  dbms_utility.compile_schema(schema => 'BOOK_RENT_SYSTEM');
END;
/

PROMPT installing TABLE DATA...
@./inserts/insert_book_categories.sql
@./inserts/insert_book_locations.sql
@./inserts/insert_users.sql
@./inserts/insert_rentable_books.sql
@./inserts/insert_rental_details.sql

PROMPT Done.

