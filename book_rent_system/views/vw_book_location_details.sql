CREATE VIEW vw_book_location_details AS
SELECT rb.book_id, rb.author, rb.book_title, bl.location_id, bl.zip_code || ', ' || bl.city || ' ' || bl.street_name || ' ' ||  bl.building_number || '.' AS "Address"
FROM book_locations bl JOIN rentable_books rb on (bl.location_id = rb.location_id)
ORDER BY rb.book_id;
