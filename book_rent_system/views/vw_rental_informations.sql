CREATE VIEW vw_rental_informations AS
SELECT DISTINCT us.last_name || ' ' || us.first_name AS "Nev", rb.book_id, rb.author, rb.book_title, bc.category_id, bc.category_name, bc.cost_per_day, rd.start_date, rd.end_date, rd.end_date - rd.start_date as "Osszes nap", (rd.end_date - rd.start_date) * bc.cost_per_day as "Osszesen fizetendo (Ft)"  
FROM book_locations bl JOIN rentable_books rb on (bl.location_id = rb.location_id) JOIN book_categories bc on (bc.category_id = rb.category_id) JOIN rental_details rd on (rd.book_id = rb.book_id) JOIN users us on (us.user_id = rd.user_id)
ORDER BY "Nev";
