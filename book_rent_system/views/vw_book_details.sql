CREATE VIEW vw_book_details AS
SELECT rb.book_id, rb.author, rb.book_title, bc. category_id, bc.category_name, rb.topic, rb.country, rb.page, rb.year, bc.cost_per_day
FROM book_categories bc JOIN rentable_books rb on (bc.category_id = rb.category_id)
ORDER BY rb.book_id;
